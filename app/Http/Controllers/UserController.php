<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Adldap\AdldapInterface;

class UserController extends Controller
{
    protected $ldap;

    public function __construct(AdldapInterface $ldap)
    {
        $this->ldap = $ldap;
    }

    public function index()
    {
        $users = $this->ldap->search()->users()->get();

        return view('users.index', compact('users'));
    }
}
